import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {IBook} from '../../interfaces/IBook';
import {BookAndCdService} from '../../services/book-and-cd.service';
import {ICD} from '../../interfaces/ICD';

@Component({
  selector: 'app-add-cd',
  templateUrl: './add-cd.page.html',
  styleUrls: ['./add-cd.page.scss'],
})
export class AddCdPage implements OnInit {

  constructor(private modalCtrl: ModalController, private bookAndCdService: BookAndCdService) { }

  ngOnInit() {
  }

  addBtnTapped(form) {
    const cd: ICD = {
      id: this.bookAndCdService.getNextCDID(),
      title: form.value.title,
      artist: form.value.artist,
      genre: form.value.genre,
      releaseDate: form.value.relatedAddress,
      lended: false
    };
    this.bookAndCdService.addCD(cd);
    this.quitModal();
  }

  quitModal() {
    this.modalCtrl.dismiss();
  }

}
