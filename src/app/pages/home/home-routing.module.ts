import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: 'book',
        loadChildren: () => import('../book-list-page/book-list-page.module').then(m => m.BookListPagePageModule)
      },
      {
        path: 'cd',
        loadChildren: () => import('../cd-list-page/cd-list-page.module').then(m => m.CdListPagePageModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('../settings-page/settings-page.module').then(m => m.SettingsPagePageModule)
      },
      {
        path: '',
        redirectTo: '/home/book',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
