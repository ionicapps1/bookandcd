import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LendCdPagePage } from './lend-cd-page.page';

const routes: Routes = [
  {
    path: '',
    component: LendCdPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LendCdPagePageRoutingModule {}
