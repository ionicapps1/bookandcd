import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LendCdPagePageRoutingModule } from './lend-cd-page-routing.module';

import { LendCdPagePage } from './lend-cd-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LendCdPagePageRoutingModule
  ],
  declarations: [LendCdPagePage]
})
export class LendCdPagePageModule {}
