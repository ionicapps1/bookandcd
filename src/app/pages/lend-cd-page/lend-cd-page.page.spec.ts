import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LendCdPagePage } from './lend-cd-page.page';

describe('LendCdPagePage', () => {
  let component: LendCdPagePage;
  let fixture: ComponentFixture<LendCdPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LendCdPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LendCdPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
