import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BookAndCdService} from '../../services/book-and-cd.service';
import {ICD} from '../../interfaces/ICD';

@Component({
    selector: 'app-lend-cd-page',
    templateUrl: './lend-cd-page.page.html',
    styleUrls: ['./lend-cd-page.page.scss'],
})
export class LendCdPagePage implements OnInit {

    public cd: ICD;

    constructor(private route: ActivatedRoute, private bookAndCdService: BookAndCdService) {
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        this.getCD(id);
    }

    buttonClicked(id) {
        this.bookAndCdService.lendAndRetrieveCD(id);
        this.getCD(id);
    }

    getCD(id) {
        this.cd = this.bookAndCdService.getCD(id);
    }
}
