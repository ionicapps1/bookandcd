import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BookAndCdService} from '../../services/book-and-cd.service';
import {IBook} from '../../interfaces/IBook';

@Component({
    selector: 'app-lend-book-page',
    templateUrl: './lend-book-page.page.html',
    styleUrls: ['./lend-book-page.page.scss'],
})
export class LendBookPagePage implements OnInit {

    book: IBook;

    constructor(private route: ActivatedRoute, private bookAndCdService: BookAndCdService) {
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        this.getBook(id);
    }

    buttonClicked(id) {
        this.bookAndCdService.lendAndRetrieveBook(id);
        this.getBook(id);
    }

    getBook(id) {
        this.book = this.bookAndCdService.getBook(id);
    }

}
