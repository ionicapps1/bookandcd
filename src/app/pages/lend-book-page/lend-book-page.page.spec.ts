import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LendBookPagePage } from './lend-book-page.page';

describe('LendBookPagePage', () => {
  let component: LendBookPagePage;
  let fixture: ComponentFixture<LendBookPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LendBookPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LendBookPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
