import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LendBookPagePageRoutingModule } from './lend-book-page-routing.module';

import { LendBookPagePage } from './lend-book-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LendBookPagePageRoutingModule
  ],
  declarations: [LendBookPagePage]
})
export class LendBookPagePageModule {}
