import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LendBookPagePage } from './lend-book-page.page';

const routes: Routes = [
  {
    path: '',
    component: LendBookPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LendBookPagePageRoutingModule {}
