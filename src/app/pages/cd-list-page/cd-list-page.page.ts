import { Component, OnInit } from '@angular/core';
import {ICD} from '../../interfaces/ICD';
import {BookAndCdService} from '../../services/book-and-cd.service';
import {ModalController} from '@ionic/angular';
import {AddCdPage} from '../add-cd/add-cd.page';

@Component({
  selector: 'app-cd-list-page',
  templateUrl: './cd-list-page.page.html',
  styleUrls: ['./cd-list-page.page.scss'],
})
export class CdListPagePage implements OnInit {

  public cds: ICD[];

  constructor(private bookAndCdService: BookAndCdService, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.bookAndCdService.getCDs().then(data => {
      this.cds = data;
    });
  }

  refresh(event) {
    this.bookAndCdService.getCDs().then(data => {
      this.cds = data;
      event.target.complete();
    });
  }
}
