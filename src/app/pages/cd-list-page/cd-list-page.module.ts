import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CdListPagePageRoutingModule } from './cd-list-page-routing.module';

import { CdListPagePage } from './cd-list-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CdListPagePageRoutingModule
  ],
  declarations: [CdListPagePage]
})
export class CdListPagePageModule {}
