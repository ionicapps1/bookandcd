import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CdListPagePage } from './cd-list-page.page';

describe('CdListPagePage', () => {
  let component: CdListPagePage;
  let fixture: ComponentFixture<CdListPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdListPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CdListPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
