import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CdListPagePage } from './cd-list-page.page';

const routes: Routes = [
  {
    path: '',
    component: CdListPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CdListPagePageRoutingModule {}
