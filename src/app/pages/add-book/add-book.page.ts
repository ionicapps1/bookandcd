import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {IBook} from '../../interfaces/IBook';
import {BookAndCdService} from '../../services/book-and-cd.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.page.html',
  styleUrls: ['./add-book.page.scss'],
})
export class AddBookPage implements OnInit {

  constructor(private modalCtrl: ModalController, private bookAndCdService: BookAndCdService) { }

  ngOnInit() {
  }

  addBtnTapped(form) {
    const book: IBook = {
      id: this.bookAndCdService.getNextBookID(),
      title: form.value.title,
      overview: form.value.overview,
      author: form.value.author,
      genre: form.value.genre,
      releaseDate: form.value.relatedAddress,
      lended: false
    };
    this.bookAndCdService.addBook(book);
    this.quitModal();
  }

  quitModal() {
    this.modalCtrl.dismiss();
  }
}
