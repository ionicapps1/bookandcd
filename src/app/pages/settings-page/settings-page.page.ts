import { Component, OnInit } from '@angular/core';
import {AddBookPage} from '../add-book/add-book.page';
import {IonRouterOutlet, ModalController} from '@ionic/angular';
import {AddCdPage} from '../add-cd/add-cd.page';

@Component({
  selector: 'app-settings-page',
  templateUrl: './settings-page.page.html',
  styleUrls: ['./settings-page.page.scss'],
})
export class SettingsPagePage implements OnInit {

  constructor(private modalCtrl: ModalController, private routerOutlet: IonRouterOutlet) { }

  ngOnInit() {
  }

  async addBook() {
    const modal = await this.modalCtrl.create({
      component: AddBookPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    await modal.present();
  }

  async addCd() {
    const modal = await this.modalCtrl.create({
      component: AddCdPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    await modal.present();
  }

}
