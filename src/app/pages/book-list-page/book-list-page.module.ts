import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookListPagePageRoutingModule } from './book-list-page-routing.module';

import { BookListPagePage } from './book-list-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookListPagePageRoutingModule
  ],
  declarations: [BookListPagePage]
})
export class BookListPagePageModule {}
