import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookListPagePage } from './book-list-page.page';

describe('BookListPagePage', () => {
  let component: BookListPagePage;
  let fixture: ComponentFixture<BookListPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookListPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookListPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
