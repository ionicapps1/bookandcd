import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookListPagePage } from './book-list-page.page';

const routes: Routes = [
  {
    path: '',
    component: BookListPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookListPagePageRoutingModule {}
