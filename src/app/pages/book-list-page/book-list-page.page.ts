import {Component, OnInit} from '@angular/core';
import {BookAndCdService} from '../../services/book-and-cd.service';
import {ICD} from '../../interfaces/ICD';
import {IBook} from '../../interfaces/IBook';
import {ModalController, NavController} from '@ionic/angular';
import {AddBookPage} from '../add-book/add-book.page';

@Component({
    selector: 'app-book-list-page',
    templateUrl: './book-list-page.page.html',
    styleUrls: ['./book-list-page.page.scss'],
})
export class BookListPagePage implements OnInit {

    books: IBook[];

    constructor(public bookAndCdService: BookAndCdService, public navCtrl: NavController, public modalCtrl: ModalController) {
    }

    ngOnInit() {
        this.bookAndCdService.getBooks().then(data => {
            this.books = data;
        });
    }

    refresh(event) {
        this.bookAndCdService.getBooks().then(data => {
            this.books = data;
            event.target.complete();
        });
    }
}
