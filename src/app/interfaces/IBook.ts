export class IBook {
    id: number;
    title: string;
    overview: string;
    author: string;
    releaseDate: Date;
    genre: string;
    lended: boolean;
}
