export class ICD {
    id: number;
    title: string;
    genre: string;
    artist: string;
    releaseDate: Date;
    lended: boolean;
}
