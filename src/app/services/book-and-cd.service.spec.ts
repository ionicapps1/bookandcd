import { TestBed } from '@angular/core/testing';

import { BookAndCdService } from './book-and-cd.service';

describe('BookAndCdService', () => {
  let service: BookAndCdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BookAndCdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
