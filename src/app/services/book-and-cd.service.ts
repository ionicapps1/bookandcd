import {Injectable} from '@angular/core';
import {ICD} from '../interfaces/ICD';
import {IBook} from '../interfaces/IBook';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class BookAndCdService {

    constructor(private storage: Storage) {
    }

    cds: ICD[] = [];

    books: IBook[] = [];

    private BOOK_KEY = 'book_';
    private CD_KEY = 'cd_';

    addBook(book: IBook) {
        this.books.push(book);
        this.storage.set(this.getBookKey(book), JSON.stringify(book));
    }

    addCD(cd: ICD) {
        this.cds.push(cd);
        console.log(this.getCDKey(cd));
        this.storage.set(this.getCDKey(cd), JSON.stringify(cd));
    }

    lendAndRetrieveBook(id) {
        const book = this.books.find(b => b.id == id);
        book.lended = !book.lended;
        this.storage.set(this.getBookKey(book), JSON.stringify(book));
    }

    lendAndRetrieveCD(id) {
        const cd = this.cds.find(b => b.id == id);
        cd.lended = !cd.lended;
        this.storage.set(this.getCDKey(cd), JSON.stringify(cd));
    }

    getBook(id) {
        return this.books.find(b => b.id == id);
    }

    getBookKey(book: IBook) {
        return this.BOOK_KEY + book.id.toString();
    }

    getBooks(): Promise<IBook[]> {
        return new Promise<IBook[]>(resolve => {
            const results: IBook[] = [];
            this.storage.keys().then(keys =>
                keys
                    .filter(key => key.includes(this.BOOK_KEY))
                    .forEach(key =>
                        this.storage.get(key).then(data => {
                            results.push(JSON.parse(data));
                            this.books.push(JSON.parse(data));
                        })
                    )
            );
            return resolve(results);
        });
    }

    getCD(id) {
        return this.cds.find(b => b.id == id);
    }

    getCDKey(cd: ICD) {
        return this.CD_KEY + cd.id.toString();
    }

    getCDs(): Promise<ICD[]> {
        return new Promise<ICD[]>(resolve => {
            const results: ICD[] = [];
            this.storage.keys().then(keys =>
                keys
                    .filter(key => key.includes(this.CD_KEY))
                    .forEach(key =>
                        this.storage.get(key).then(data => {
                            results.push(JSON.parse(data));
                            this.cds.push(JSON.parse(data));
                        })
                    )
            );
            return resolve(results);
        });
    }

    getNextBookID() {
        console.log(this.books.length + 1);
        return this.books.length + 1;
    }

    getNextCDID() {
        console.log(this.cds.length + 1);
        return this.cds.length + 1;
    }
}
