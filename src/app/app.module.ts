import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BookAndCdService} from './services/book-and-cd.service';
import {AddBookPage} from './pages/add-book/add-book.page';
import {AddCdPage} from './pages/add-cd/add-cd.page';
import {AddBookPageModule} from './pages/add-book/add-book.module';
import {AddCdPageModule} from './pages/add-cd/add-cd.module';
import {IonicStorageModule} from '@ionic/storage';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [BrowserModule, IonicModule.forRoot(), IonicStorageModule.forRoot(), AppRoutingModule, AddBookPageModule, AddCdPageModule],
    providers: [
        StatusBar,
        SplashScreen,
        BookAndCdService,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
