import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'book-list-pages',
    loadChildren: () => import('./pages/book-list-page/book-list-page.module').then(m => m.BookListPagePageModule)
  },
  {
    path: 'cd-list-pages',
    loadChildren: () => import('./pages/cd-list-page/cd-list-page.module').then(m => m.CdListPagePageModule)
  },
  {
    path: 'lend-book-pages/:id',
    loadChildren: () => import('./pages/lend-book-page/lend-book-page.module').then(m => m.LendBookPagePageModule)
  },
  {
    path: 'lend-cd-pages/:id',
    loadChildren: () => import('./pages/lend-cd-page/lend-cd-page.module').then(m => m.LendCdPagePageModule)
  },
  {
    path: 'settings-pages',
    loadChildren: () => import('./pages/settings-page/settings-page.module').then(m => m.SettingsPagePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
